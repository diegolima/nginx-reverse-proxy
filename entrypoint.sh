#!/bin/bash
if [ -z "$DESTINATION_ADDRESS" ]; then
    echo "[ERROR] DESTINATION_ADDRESS was not provided."
    exit 1
fi
test -z "$DESTINATION_PORT" && export DESTINATION_PORT=80 && echo "[WARNING] DESTINATION_PORT was not provided. Defaulting to $DESTINATION_PORT"
test -z "$HTTP_PORT" && export HTTP_PORT=80
test -z "$SERVER_NAME" && export SERVER_NAME=$(hostname)
test -z "$PROXY_REDIRECT" && export PROXY_REDIRECT="default"
test -z "$PROXY_SET_HEADER" && export PROXY_SET_HEADER='Host $host'
test -z "$LOCATION" && export LOCATION='/'

if [ -z "$REWRITE_RULE" ]; then
    DOLLAR='$' envsubst < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf
else
    DOLLAR='$' envsubst < /etc/nginx/conf.d/default_rewrite.conf.template > /etc/nginx/conf.d/default.conf
fi
nginx -g "daemon off;"