FROM nginx:latest
COPY reverse_proxy.conf /etc/nginx/conf.d/default.conf.template
COPY reverse_proxy_rewrite.conf /etc/nginx/conf.d/default_rewrite.conf.template
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

CMD [ "/entrypoint.sh" ]